<!-- .slide: data-background="#ffffff" -->

# Architecture Best Practices

Common Guidance for Content Platforms Engineering

---slide---
<!-- .slide: data-background="#ffffff" -->

## This will cover:

- Areas of Technology & Guidance<!-- .element: class="fragment" data-fragment-index="1" -->
- Application of Guidance <!-- .element: class="fragment" data-fragment-index="2" -->
- Interactions Between Systems <!-- .element: class="fragment" data-fragment-index="3" -->

---slide---
<!-- .slide: data-background="#ffffff" -->

## Areas of Technology

This covers how we want to achieve consistency as we consolidate and build out new workflows and services across content platforms.  The guidance takes into account scalability and resiliency needs, while also allowing for easy adoption.

---slide---
<!-- .slide: data-background="#ffffff" -->

### Technology Guidance

![Technology and Guidance Table](assets/images/arch_guidance_2-2.png)


---slide---
<!-- .slide: data-background="#ffffff" -->

## Common Building Blocks

These building blocks show how the architecture is broken up to support the various areas of a workflow. This is meant to convey that our patterns are reusable and that services and APIs can be interchangeable when a workflow calls for it.


---slide---
<!-- .slide: data-background="#ffffff" -->

### Common Building Blocks

![Architecture Building Blocks](assets/images/high_level.png)


---slide---
<!-- .slide: data-background="#ffffff" -->

## Universal View
The Universal view takes the technology areas of guidance and the building blocks and shows how they will all interact with each other at a high level across services both new and old.

---slide---
<!-- .slide: data-background="#ffffff" -->
![alt text](assets/images/universal_view.png)


---slide---
<!-- .slide: data-background="#ffffff" -->

## Example Workflow

The upcoming workflow shows how a user accesses an application and the underlying frameworks, languages, and infrastructure needed to support it. 

---slide---
<!-- .slide: data-background="#ffffff" -->

### Example Workflow

![Web App Workflow](assets/images/WebWorkflowExample.png)


---slide---
<!-- .slide: data-background="#ffffff" -->

## Authentication
For user authentication and authorization needs, we are leveraging known services MyId, Keystone, and SOLO. A separate layer for session management was created using AWS Cognito. This flow allows easy adoption by any service to meet the authentication and authorization needs for both internal and external users.


---slide---
<!-- .slide: data-background="#ffffff" -->

### Authentication

![Authentication Architecture](assets/images/auth_example.png)

---slide---
<!-- .slide: data-background="#ffffff" -->

## Compute - Front End
FITT is architected to not only provide those same features, plus an HTTP request/response caching, connection keep-alive, and build features designed for scalable, content heavy projects. In the diagram you will see why FITT is the Front End standard as it embodies many of the guidance suggested for all architectures.

---slide---
<!-- .slide: data-background="#ffffff" -->

### Compute - Front End

![FITT Architecture](assets/images/FITT.png)

---slide---
<!-- .slide: data-background="#ffffff" -->

## Compute - Middle/Back End

The goal is to use Java when building out the back-end. Kotlin is also an approved choice, which may be more attractive for some teams. 

Additionally NodeJS is can be permitted for some back-end after proper vetting, however, it should not be used to connect to persistence layers.

Note: We do not use NodeJS to connect to persistence layers.


---slide---
<!-- .slide: data-background="#ffffff" -->

### Compute - Middle/Back End

![BE Architecture](assets/images/BEDiagram.png)

---slide---
<!-- .slide: data-background="#ffffff" -->

## Workflow Orchestration

Workflow Orchestration is essential in increasing consistency in our workflows and trust in our data. By providing a centralized orchestration layer that leverages plug and play tasks and a central event bus we can begin to automate repeatable tasks with confidence and provide opportunities to enact on new workflows that were previously unobtainable by sharing tasks.

---slide---
<!-- .slide: data-background="#ffffff" -->

### Workflow Orchestration

![Workflow Architecture](assets/images/orchestration.png)


---slide---
<!-- .slide: data-background="#ffffff" -->

## Observability

Observability is crucial in understanding the health of our services on a day to day basis. Beyond that, it is also required to allow us to make the right decisions for the future. By monitoring, logging and reporting how our systems and content are used we can better plan and execute on our needs.

---slide---
<!-- .slide: data-background="#ffffff" -->

### Observability

![Observability View](assets/images/observability.png)


---slide---
<!-- .slide: data-background="#ffffff" -->

## CI/CD

For CI/CD we are going to leverage Gitlab CI/CD pipelines. This will give us fast, autonomous continues delivery of our features.

---slide---
<!-- .slide: data-background="#ffffff" -->

### CI/CD

![alt text](assets/images/cicd.png)


---slide---
<!-- .slide: data-background="#ffffff" -->

## Summary

This is the way or rm rf your code base